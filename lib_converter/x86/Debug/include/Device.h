#pragma once

#include "Channel.h"
#include <memory>

//���������� �������� �����������
class DeviceImp;

/**
* \brief ������������ ������ ���������� ����������� �����������
*/
class Device
{
public:
	~Device();

	/**
	* \brief ������� ���������� �� ������ ���� ��������������� vid\pid
	* \param pid ������������� ��������
	* \param vid ������������� �������������
	*/
	static std::unique_ptr<Device> instance(uint32_t pid, uint32_t vid);


	/**
	* \brief �������� ������ � ������ ���������� �� ��������� ������.
	* ���������� ���������� Exception::ChannelNumber ��� ��������� ������
	* \param numder
	* \return ������ �� �����
	*/
	Channel& channel(uint8_t number) const;


	/**
	* \brief ���������� ���������� �������������� �������
	*/
	uint32_t numChannels() const;

private:
	Device(uint32_t pid, uint32_t vid);

	std::unique_ptr<DeviceImp> _imp;
};

