#pragma once
#include "Connectors.h"
#include <memory>

//���������� �������� �����������
class Invoker;
class ChannelImp;

/**
* \brief ���������� ������ ������ ����������
*/
class Channel
{
public:
	Channel(uint8_t index, Invoker& invoker);

	Channel& operator=(Channel const&) = delete;

	Channel(Channel const&) = delete;

	Channel(Channel&& r) noexcept;

	~Channel();

	/**
	* \brief ���������� ���������� ��������� ������ ������
	* \param proc
	* \return
	*/
	bool connect(DataProcessor * proc) const;


	/**
	* \brief ����������� ���������� ��������� ������ ������
	* \param proc
	* \return
	*/
	bool disconnect(DataProcessor* proc) const;


	/**
	* \brief �������� ����� ����������
	* \return
	*/
	bool turnOn() const;


	/**
	* \brief ��������� ����� ����������
	* \return
	*/
	bool turnOff() const;


	/**
	* \brief ���������� ��������������� ������
	* \param new_scale
	* \return
	*/
	Scale scale(Scale new_scale) const;


	/**
	* \brief �������� �������� �������� ������
	* \param
	* \return
	*/
	Scale scale() const;


	/**
	* \brief ������������� ������� ������������� ������
	* \param new_sr
	* \return
	*/
	SampleRate sampleRate(SampleRate new_sr) const;


	/**
	* \brief �������� �������� ������� ������������� ������
	* \return
	*/
	SampleRate sampleRate() const;


	/**
	* \brief �������� ���������� ����� ������
	* \return
	*/
	uint8_t index() const;

private:
	std::unique_ptr<ChannelImp> _imp;
};

