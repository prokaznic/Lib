#pragma once
#include <cstdint>

enum class SampleRate : uint32_t {
	SR_250K = 250000,
	SR_200K = 200000,
	SR_100K = 100000,
	SR_10K = 10000,
	SR_1K = 1000,
	SR_100 = 100,
	SR_10 = 10
};

enum class Scale : uint32_t {
	SC_1 = 1,
	SC_2 = 2,
	SC_5 = 5,
	SC_10 = 10,
	SC_20 = 20
};

enum class ChSwitch : uint8_t {
	on,
	off
};

#pragma pack(push,1)
typedef struct _ChannelSetting {
	uint8_t ch_idx;
	SampleRate samplerate;
	Scale scale;
}SettingBlock;

#pragma pack(pop)