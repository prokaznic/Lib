#pragma once
#include <memory>
#include "interfaceImp.h"

//��������������� ����������

enum class WatchType;

//���������� �������� �����������
class DeviceWatcherImp;
class NoticeImp;
class DeviceHandleImp;
class Device;

class DeviceWatcher {
	//��������� ������������ �������� ���������� ������
	friend std::unique_ptr<DeviceWatcher>::deleter_type;
	friend interface HardwareObserver;

	/**
	 * \brief ������������ ������ ����������� ��� ��������� ��������� � ��������� ����������������
	 */
	class Notice{
		friend class DeviceWatcher;
	public:
		explicit Notice(NoticeImp& imp);
		/**
		 * \brief ���������� ����������� �� ��������� �����������
		 * \param observer 
		 * \param type 
		 */
		void connect(HardwareObserver* observer, WatchType type) const;

		/**
		 * \brief ��������� ����������� �� �����������
		 * \param observer 
		 */
		void disconnect(HardwareObserver* observer) const;

	private:
		
		Notice(Notice&&) = default;
		NoticeImp& _imp;
	};

public:

	/**
	 * \brief ����������� ����������
	 */
	class Handle {

	public:
		Handle(uint32_t id, NoticeImp* family_node_imp);
		~Handle();
		Handle(Handle&&) = default;

		/**
		 * \brief ��������� ������������� ����������
		 * \return ������ ����������
		 */
		std::unique_ptr<Device> open() const;
		/**
		 * \brief �������� id ���������� � ��������� ��������� � ��������� ����������������
		 * \return id ����������
		 */
		uint32_t id() const;

	private:
		std::unique_ptr<DeviceHandleImp> _imp;
	};

	/**
	 * \brief ���������� ����������� ��� ��������� ��������� � ��������� ����������������
	 * \param vid 
	 * \param pid 
	 * \return ����������� ��� ��������� ���������
	 */
	Notice notice(uint32_t pid, uint32_t vid) const;

	/**
	 * \brief �������� ������ �� ��������� ����������� �� ������������
	 * \return DeviceWatcher&
	 */
	static DeviceWatcher& instance();

private:
	~DeviceWatcher();
	DeviceWatcher();

	std::unique_ptr<DeviceWatcherImp> _imp;
};