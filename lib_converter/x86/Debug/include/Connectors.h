#pragma once

#include <vector>
#include "interfaceImp.h"
#include "ChannelAttribute.h"
#include "DeviceWatcher.h"

/**
 * \brief	��������� ����������� ��������� ������ ������. 
 *			������������ � ������ �� ��������� ������.
 */
interface DataProcessor{

	/**
	 * \brief ����� ��������� ������ ����������� �� ������ ����������
	 * \param data ������ �������� �������
	 */
	virtual void __stdcall process(const std::vector<double> &data) = 0;
	/**
	 * \brief ������������ ����� ����������� ������ 
	 */
	virtual void __stdcall reset() = 0;
	/**
	 * \brief ������������ ���������� ����������� ������ �� ��������� ���������� ������
	 * \param current ������� ��������� ������
	 */
	virtual void __stdcall notify(const SettingBlock& current)=0;
};

/**
* \brief ��������� ����������� �� ������������ ����������
*/
interface HardwareObserver{
	virtual void __stdcall hardwareConnected(DeviceWatcher::Handle node) = 0;
	virtual void __stdcall hardwareDisconnected() = 0;
};

/**
 * \brief ���������� ��� ����������.
 * \remark Single ������������ ��� ����������� �� ���������� ���������� ��������� ����
 * \remark Family ������������ ��� ����������� �� ��������� ��������� ��������� ����
 */
enum class WatchType {
	Single,
	Family
};

